from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop
from aiohttp.pytest_plugin import aiohttp_client
from src.server import app

class TestServer(AioHTTPTestCase):
    async def get_application(self):
        return app

    @unittest_run_loop
    async def test_handle_root_request(self, aiohttp_client):
        client = await aiohttp_client(app)
        response = await client.get('/')
        assert response.status == 200

    @unittest_run_loop
    async def test_handle_websocket_connection(self, aiohttp_client):
        client = await aiohttp_client(app)
        ws = await client.ws_connect('/ws')
        await ws.send_str("captureImage")
        await ws.close()
