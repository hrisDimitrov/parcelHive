class MouseTracker {
  constructor(url) {
    this.ws = new WebSocket(url);
    this.initWebSocket();
    this.initEvents();
  }

  initWebSocket() {
    this.ws.onopen = (e) => {
      console.log("Connection established!");
    };

    this.ws.onerror = (error) => {
      console.error(`WebSocket Error ${error}`);
    };

    this.ws.onmessage = this.handleIncomingMessage.bind(this);
  }

  handleIncomingMessage(event) {
    const data = JSON.parse(event.data);
    document.getElementById('x-coor').innerText = data.x;
    document.getElementById('y-coor').innerText = data.y;
  }

  initEvents() {
    document.addEventListener("mousemove", this.handleMouseMove.bind(this));
    document
      .getElementById("captureButton")
      .addEventListener("click", this.handleButtonClick.bind(this));
  }

  handleMouseMove(event) {
    const x = event.clientX;
    const y = event.clientY;

    const mouseCoordinates = JSON.stringify({
      x: x,
      y: y,
    });

    this.ws.send(mouseCoordinates);
  }

  handleButtonClick() {
    this.ws.send("captureImage");
  }
}

const mouseTracker = new MouseTracker("ws://localhost:8080/ws");
