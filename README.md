# ParcelHive

## Name
Mouse movement visualizer - ParcelHive

## Description
Application that captures the movement of the mouse and visualizes the coordinates. Users can also capture images at specific points. It uses Python, aiohttp for handling web requests, and OpenCV for capturing images.

## Features
-Real-time tracking of mouse movement.<br/>
-Capture images at specific coordinates.<br/>
-Save images with associated x and y coordinates.

## Requirements
-Python 3.10 or higher.<br/>
-Various Python libraries, listed in requirements.txt.

## Installation
### Clone the Repository
```bash
git clone https://github.com/your-username/ParcelHive.git
cd ParcelHive
```
### Create a Virtual Environment (Optional but Recommended)
```bash
python -m venv venv
source venv/bin/activate
```
### Install Dependencies
```bash
pip install -r requirements.txt
```

## Usage
### Start the Server
```bash
python server.py
```

### Open the Application
Navigate to http://localhost:8080 in your web browser.

## Testing
Tests are located in the tests directory. You can run them using pytest:<br/>
```bash
pytest tests/
```

## Preview - GIF
![Working preview gif](mmv_parcelhive.gif)

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Authors and acknowledgment
Hristos Dimitrov
x