import cv2
import json
import sqlite3
import database
from aiohttp import web
from pathlib import Path

cap = cv2.VideoCapture(0)
conn = sqlite3.connect("database.db")
cursor = conn.cursor()

async def handle_root_request(request):
    return web.FileResponse(str(Path.cwd()) + "/static/main.html")

async def handle_websocket_connection(request):
    ws = web.WebSocketResponse()
    await ws.prepare(request)
    coordinates = None
    coorToInsert = None

    async for msg in ws:
        if msg.type == web.WSMsgType.TEXT:
            if msg.data == "captureImage":
                print("Capturing image")
                print("Coordinates of the mouse on capture ->", coordinates)
                ret, frame = cap.read()
                if ret:
                    coorToInsert = json.loads(coordinates)
                    x = coorToInsert["x"]
                    y = coorToInsert["y"]
                    cv2.imwrite("captured_image.png", frame)
                    cap.release()
                    image_path = str(Path.cwd()) + "\\" + "captured_image.png" 
                    cursor.execute("INSERT INTO mouse_xy_img_path (x, y, image_path) VALUES (?, ?, ?)", (x, y, image_path))
                    conn.commit()
            else:
                coordinates = msg.data
                await ws.send_str(coordinates)

    return ws

app = web.Application()
app.router.add_get("/", handle_root_request)
app.router.add_get("/ws", handle_websocket_connection)
app.router.add_static("/static/", path="static", name="static")

web.run_app(app)
